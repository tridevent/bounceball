static var agrandar:boolean;
static var achicar:boolean;
static var check:short=0;
private var contador:int=0; 
var izquierda:float=0;
var derecha:float=0;
var valores:float[];//barra:esc _ izq:pos,esc _ der:pos,esc 
//-------------------------------------
var texturaNormal:Texture;
var texturaGrande:Texture;
var texturaChica:Texture;
//-------------------------------------
var pelota:Transform;
var latizq:GameObject;
var latder:GameObject;
var barra:GameObject;
//-----------------------------------------------------------------------------------------------//
function Start(){
latizq= GameObject.Find("izquierda");
latder= GameObject.Find("derecha");
barra= GameObject.Find("Barra");
//--------------------
agrandar=false;
achicar= false;
check=0;
InvokeRepeating("duracion",1,1);
InvokeRepeating("Resolucion",1,3600);
}
//-----------------------------------------------------------------------------------------------//
function Resolucion()
{
valores[0]=barra.transform.GetComponent(ResBarra).escX;
valores[1]=latizq.transform.GetComponent(ResDesproporcionado).posX;
valores[2]=latizq.transform.GetComponent(ResDesproporcionado).escX;
valores[3]=latder.transform.GetComponent(ResDesproporcionado).posX;
valores[4]=latder.transform.GetComponent(ResDesproporcionado).escX;
//--------------------
izquierda=(valores[0]*4)+valores[1]+valores[2];//normal (8)
derecha=((valores[0]*4)+valores[4]-valores[3])*(-1);
GetComponent.<Renderer>().material.mainTexture= texturaNormal;
}
//-----------------------------------------------------------------------------------------------//
function Update () {
	if(Input.touchCount>0)
	{
	ActualizarPosicionBarra(Input.GetTouch(0).position);
	}
	else if(Input.GetMouseButton(0))
		{
		ActualizarPosicionBarra(Input.mousePosition);
		}
}
//-----------------------------------------------------------------------------------------------//
function duracion()
{
if(check==0)
	{
	if(agrandar== false && achicar == true)
		{
		transform.localScale.x -=2;
		valores[0]=barra.transform.GetComponent(ResBarra).escX;
		izquierda=(valores[0]*3)+valores[1]+valores[2];//achicar (6)
		derecha=((valores[0]*3)+valores[4]-valores[3])*(-1);
		check=2;
		GetComponent.<Renderer>().material.mainTexture= texturaChica;
		}
	if(agrandar== true && achicar == false)
		{
		transform.localScale.x +=2;
		valores[0]=barra.transform.GetComponent(ResBarra).escX;
		izquierda=(valores[0]*5)+valores[1]+valores[2];//agrandar (10)
		derecha=((valores[0]*5)+valores[4]-valores[3])*(-1);
		check=1;
		GetComponent.<Renderer>().material.mainTexture= texturaGrande;
		}
	}
//-----------------------------------------------//		
if(check==1)
	{
	contador++;
	if(contador==10)
		{
		check=0;
		transform.localScale.x -=2;
		agrandar=false;
		contador=0;
		valores[0]=barra.transform.GetComponent(ResBarra).escX;
		izquierda=(valores[0]*4)+valores[1]+valores[2];//normal (8)
		derecha=((valores[0]*4)+valores[4]-valores[3])*(-1);
		GetComponent.<Renderer>().material.mainTexture= texturaNormal;
		}
	}
if(check==2)
	{
	contador++;
	if(contador==10)
		{
		check=0;
		transform.localScale.x +=2;
		achicar=false;
		contador=0;
		valores[0]=barra.transform.GetComponent(ResBarra).escX;
		izquierda=(valores[0]*4)+valores[1]+valores[2];//normal (8)
		derecha=((valores[0]*4)+valores[4]-valores[3])*(-1);
		GetComponent.<Renderer>().material.mainTexture= texturaNormal;
		}
	}
}
//-----------------------------------------------------------------------------------------------//
function ActualizarPosicionBarra(posicion: Vector2)
{
var hit:RaycastHit;
var ray:Ray;
ray=Camera.main.ScreenPointToRay(posicion);

transform.position.x=Mathf.Clamp(Camera.main.ScreenToWorldPoint(posicion).x,izquierda,derecha);
//print("1");		
if(Physics.Raycast(ray,hit,100))
	{
	//print("2");
	if(hit.transform.tag== "barra")
		{
		//print("acaaaaa");
		if(ZonaDeMuerte.dormir==true)
			{
			pelota.parent=null;
			pelota.GetComponent.<Rigidbody>().AddForce(Vector3(Random.Range(0,100)>50 ? 200: -200,-200,0),ForceMode.Impulse);
			ZonaDeMuerte.dormir=false;
			}
		}
	}
}
//-----------------------------------------------------------------------------------------------//